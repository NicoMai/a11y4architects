import { Component } from '@angular/core';
import { faBraille } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'a11y4architects';
  faBraille = faBraille;
}
